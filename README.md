# game-1010-lspi



## Project description

- See PDF - Markov Decision Processes and Reinforcement Learning for Puzzles
- lspi-game-1010-python: codes of trainning AI of game 1010! by Least-Squares Policy Iteration (LSPI)
- Xcode-demo-playFullGame1010: demonstration on one example of the results

<img src="./Xcode-demo.gif" width="291" height="524" />

----
Project written in 2019 by Cindy Tang
- Note that 
    - Some parts of the python codes are contributed by another candidate, those part of codes are commented out and hidden. 
    - The python LSPI algorithm for game 1010! is not showing complete codes.


