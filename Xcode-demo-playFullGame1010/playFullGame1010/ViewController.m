//
//  ViewController.m
//  playFullGame1010
//
//  Created by Tang Sin Yee on 20/4/2019.
//  Copyright © 2019 Tang Sin Yee. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *displayBoard;
@property (strong, nonatomic) NSArray* pieces;
@property (strong, nonatomic) NSArray* queues;
@property (strong, nonatomic) NSArray* actions;
@property (nonatomic) NSInteger round;
@property (strong, nonatomic) NSMutableArray* currentPieces;
@property (strong, nonatomic) NSMutableArray* gameBoard;
@property (strong, nonatomic) NSArray* pieceCos;
@property (nonatomic) NSInteger count;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (nonatomic) NSInteger numberOfActionTaken;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (nonatomic, strong) NSArray<UIColor *> *colorForIndex;
@end

@implementation ViewController

- (void)readAllFiles {
    NSString *piecesFilePath = [[NSBundle mainBundle] pathForResource:@"allPieces" ofType:@"txt"];
    NSString *piecesString = [NSString stringWithContentsOfFile:piecesFilePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *pieces = [piecesString componentsSeparatedByString:@"\n"];
    self.pieces = [[NSArray alloc] initWithArray:pieces];
    
    NSString *queueFilePath = [[NSBundle mainBundle] pathForResource:@"allQueue" ofType:@"txt"];
    NSString *queueString = [NSString stringWithContentsOfFile:queueFilePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *queue = [queueString componentsSeparatedByString:@"\n"];
    self.queues = [[NSArray alloc] initWithArray:queue];
    
    NSString *actionFilePath = [[NSBundle mainBundle] pathForResource:@"allAction" ofType:@"txt"];
    NSString *actionString = [NSString stringWithContentsOfFile:actionFilePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *action = [actionString componentsSeparatedByString:@"\n"];
    self.actions = [[NSArray alloc] initWithArray:action];
    
}


- (void)killConsecutive {
    int numberOfLineKilled = 0;
    for (int i = 0; i< 10; i++) {
        if ([self.gameBoard[i][0] integerValue] != -1 &&
            [self.gameBoard[i][1] integerValue] != -1 &&
            [self.gameBoard[i][2] integerValue] != -1 &&
            [self.gameBoard[i][3] integerValue] != -1 &&
            [self.gameBoard[i][4] integerValue] != -1 &&
            [self.gameBoard[i][5] integerValue] != -1 &&
            [self.gameBoard[i][6] integerValue] != -1 &&
            [self.gameBoard[i][7] integerValue] != -1 &&
            [self.gameBoard[i][8] integerValue] != -1 &&
            [self.gameBoard[i][9] integerValue] != -1 ) {
            numberOfLineKilled++;
            NSArray *r = @[@-2, @-2, @-2, @-2, @-2, @-2, @-2, @-2, @-2, @-2];
            [self.gameBoard replaceObjectAtIndex:i withObject:r];
        }
        
        if ([self.gameBoard[0][i] integerValue] != -1 &&
            [self.gameBoard[1][i] integerValue] != -1 &&
            [self.gameBoard[2][i] integerValue] != -1 &&
            [self.gameBoard[3][i] integerValue] != -1 &&
            [self.gameBoard[4][i] integerValue] != -1 &&
            [self.gameBoard[5][i] integerValue] != -1 &&
            [self.gameBoard[6][i] integerValue] != -1 &&
            [self.gameBoard[7][i] integerValue] != -1 &&
            [self.gameBoard[8][i] integerValue] != -1 &&
            [self.gameBoard[9][i] integerValue] != -1) {
            numberOfLineKilled++;
            for (int k = 0; k<10; k++) {
                NSMutableArray *r2 = [[NSMutableArray alloc] initWithArray:self.gameBoard[k]];
                [r2 replaceObjectAtIndex:i withObject:@-2];
                [self.gameBoard replaceObjectAtIndex:k withObject:r2];
            }
        }
    }
    
    for (int i=0; i<10; i++) {
        for (int j=0; j<10; j++) {
            if ([self.gameBoard[i][j] integerValue] == -2) {
                NSMutableArray *r = [[NSMutableArray alloc] initWithArray:self.gameBoard[i]];
                
                [r replaceObjectAtIndex:j withObject:[NSNumber numberWithInt:-1]];
                [self.gameBoard replaceObjectAtIndex:i withObject:r];
            }
        }
    }
}

- (NSArray *)pieceCos {
    if (!_pieceCos){
        NSArray* co = @[
                        @[ @[@0, @0] ],
                        @[ @[@0,@0], @[@0,@1] ],
                        @[ @[@0,@0], @[@1,@0] ],
                        @[ @[@0,@0], @[@0,@1], @[@1,@0] ],
                        @[ @[@0,@0], @[@0,@1], @[@1,@1] ],
                        @[ @[@0,@0], @[@1,@0], @[@1,@1] ],
                        @[ @[@0,@1], @[@1,@0], @[@1,@1] ],
                        @[ @[@0,@0], @[@0,@1], @[@1,@0], @[@1,@1] ],
                        @[ @[@0,@0], @[@0,@1], @[@0,@2] ],
                        @[ @[@0,@0], @[@0,@1], @[@0,@2], @[@0,@3] ],
                        @[ @[@0,@0], @[@0,@1], @[@0,@2], @[@0,@3], @[@0,@4] ],
                        @[ @[@0,@0], @[@1,@0], @[@2,@0] ],
                        @[ @[@0,@0], @[@1,@0], @[@2,@0], @[@3,@0] ],
                        @[ @[@0,@0], @[@1,@0], @[@2,@0], @[@3,@0], @[@4,@0] ],
                        @[ @[@0,@0], @[@0,@1], @[@0,@2], @[@1,@0], @[@2,@0] ],
                        @[ @[@0,@0], @[@1,@0], @[@2,@0], @[@2,@1], @[@2,@2] ],
                        @[ @[@0,@2], @[@1,@2], @[@2,@0], @[@2,@1], @[@2,@2] ],
                        @[ @[@0,@0], @[@0,@1], @[@0,@2], @[@1,@2], @[@2,@2] ],
                        @[ @[@0,@0], @[@0,@1], @[@0,@2], @[@1,@0], @[@1,@1], @[@1,@2], @[@2,@0], @[@2,@1], @[@2,@2] ]
                        ];
        _pieceCos = [[NSArray alloc] initWithArray:co];
        
        }
    return _pieceCos;
}

- (NSArray<UIColor *> *)colorForIndex {
    if (!_colorForIndex) {
        _colorForIndex = @[[UIColor colorWithRed:106/255.0 green:90/255.0 blue:250/255.0 alpha:1],
                            [UIColor colorWithRed:255/255.0 green:215/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:255/255.0 green:215/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:154/255.0 green:205/255.0 blue:50/255.0 alpha:1],
                            [UIColor colorWithRed:255/255.0 green:140/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:255/255.0 green:105/255.0 blue:180/255.0 alpha:1],
                            [UIColor colorWithRed:139/255.0 green:0/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:255/255.0 green:140/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:255/255.0 green:105/255.0 blue:180/255.0 alpha:1],
                            [UIColor colorWithRed:139/255.0 green:0/255.0 blue:0/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:191/255.0 blue:255/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:191/255.0 blue:255/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:191/255.0 blue:255/255.0 alpha:1],
                            [UIColor colorWithRed:0/255.0 green:191/255.0 blue:255/255.0 alpha:1],
                            [UIColor colorWithRed:72/255.0 green:209/255.0 blue:204/255.0 alpha:1],
        ];
    }
    return _colorForIndex;
}


- (void)updateUI {
    int count = 0;
    for (int i=0; i<10; i++) {
        for (int j=0; j<10; j++) {
            NSInteger index = [self.gameBoard[i][j] integerValue];
            if (index >= 0 && index < 18) {
                [(UIButton *)self.displayBoard[count] setBackgroundColor:self.colorForIndex[index]];
            } else {
                [(UIButton *)self.displayBoard[count] setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            }
            count++;
        }
    }
}

- (void)updateGameBoardWithRow:(NSInteger)row Column:(NSInteger)col Value:(NSInteger)value {
    NSMutableArray *r = [[NSMutableArray alloc] initWithArray:self.gameBoard[row]];
    
    [r replaceObjectAtIndex:col withObject:@(value)];
    [self.gameBoard replaceObjectAtIndex:row withObject:r];
    
}

- (void)takeActionWithPolicy:(NSArray *)policy Pieces:(NSInteger)piece {
    
    NSInteger x = [policy[0] integerValue];
    NSInteger y = [policy[1] integerValue];
    
    NSArray *coOfpieces = [[NSArray alloc] initWithArray:self.pieceCos[piece]];
    
    for (NSArray *co in coOfpieces){
        NSInteger x_co = x + [co[0] integerValue];
        NSInteger y_co = y + [co[1] integerValue];
        [self updateGameBoardWithRow:x_co Column:y_co Value:piece];
    }
    
    
}


- (void)startARound {
    NSArray *pieces = [self.pieces[self.round] componentsSeparatedByString:@" "];
    NSArray *queue = [self.queues[self.round] componentsSeparatedByString:@" "];
    
    [self.img1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld", (long)[pieces[0] integerValue]]]];
    [self.img2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld", (long)[pieces[1] integerValue]]]];
    [self.img3 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%ld", (long)[pieces[2] integerValue]]]];
    
    double delayInSeconds = 0.15;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        // Your code here
        
   
    if (self.numberOfActionTaken == [self.actions count]-1) return;

        [UIView animateWithDuration:(0.15) animations:^{
            NSArray *policy = [self.actions[3 * self.round + self.count] componentsSeparatedByString:@" "];
            NSInteger index = [[queue objectAtIndex:self.count] integerValue];
            NSInteger piece = [[pieces objectAtIndex:index] integerValue];
            [self takeActionWithPolicy:[policy subarrayWithRange: NSMakeRange(0, 2)] Pieces:piece];
            self.countLabel.text = [NSString stringWithFormat:@"number of moves: %ld", (long)self.numberOfActionTaken];
            self.numberOfActionTaken++;
            [self updateUI];
            
            if (index == 0) {
                self.img1.image = nil;
            } else if (index == 1) {
                self.img2.image = nil;
            } else if (index == 2) {
                self.img3.image = nil;
            }
        } completion:^(BOOL finished) {
            [self killConsecutive];
      
            [UIView animateWithDuration:(0.2) animations:^{
                [self updateUI];
            } completion:^(BOOL finished) {
                if (self.numberOfActionTaken == [self.actions count]-1) return;
                self.count++;
                NSArray *policy = [self.actions[3 * self.round + self.count] componentsSeparatedByString:@" "];
                NSInteger index = [[queue objectAtIndex:self.count] integerValue];
                NSInteger piece = [[pieces objectAtIndex:index] integerValue];
            
                [self takeActionWithPolicy:[policy subarrayWithRange: NSMakeRange(0, 2)] Pieces:piece];
                self.countLabel.text = [NSString stringWithFormat:@"number of moves: %ld", (long)self.numberOfActionTaken];
                if (index == 0) {
                    self.img1.image = nil;
                } else if (index == 1) {
                    self.img2.image = nil;
                } else if (index == 2) {
                    self.img3.image = nil;
                }
                self.numberOfActionTaken++;
                
                [UIView animateWithDuration:(0.2) animations:^{
                    [self updateUI];
        
                } completion:^(BOOL finished) {
                    [self killConsecutive];
                    [UIView animateWithDuration:(0.2) animations:^{
                        
                        [self updateUI];
                    } completion:^(BOOL finished) {
                        if (self.numberOfActionTaken == [self.actions count]-1) return;
                        self.count++;
                        NSArray *policy = [self.actions[3 * self.round + self.count] componentsSeparatedByString:@" "];
                        NSInteger index = [[queue objectAtIndex:self.count] integerValue];
                        NSInteger piece = [[pieces objectAtIndex:index] integerValue];
                        
                        [self takeActionWithPolicy:[policy subarrayWithRange: NSMakeRange(0, 2)] Pieces:piece];
                        self.countLabel.text = [NSString stringWithFormat:@"number of moves: %ld", (long)self.numberOfActionTaken];
                        self.numberOfActionTaken++;
                        if (index == 0) {
                            self.img1.image = nil;
                        } else if (index == 1) {
                            self.img2.image = nil;
                        } else if (index == 2) {
                            self.img3.image = nil;
                        }
                        
                        [UIView animateWithDuration:(0.2) animations:^{
                            [self updateUI];
                            
                        } completion:^(BOOL finished) {
                            [self killConsecutive];
                            [UIView animateWithDuration:(0.2) animations:^{
                                [self updateUI];
                            } completion:^(BOOL finished) {
                                if (index == 0) {
                                    self.img1.image = nil;
                                } else if (index == 1) {
                                    self.img2.image = nil;
                                } else if (index == 2) {
                                    self.img3.image = nil;
                                };
                                 if (self.numberOfActionTaken == [self.actions count]) return;
                                self.count = 0;
                                self.round++;
                                [self startARound];
                            }];
                            
                        }];
                    }];
                }];
            }];
        }];
    });
}

- (void)viewDidLoad {
    self.round = 0;
    self.count = 0;
    [super viewDidLoad];
    [self readAllFiles];
    
    NSArray *myArray = @[
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1],
                         @[ @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1, @-1]
                         ];
    self.gameBoard = [[NSMutableArray alloc] initWithArray:myArray];
    
    [self startARound];
    
    // Do any additional setup after loading the view, typically from a nib.
}


@end
