//
//  main.m
//  playFullGame1010
//
//  Created by Tang Sin Yee on 20/4/2019.
//  Copyright © 2019 Tang Sin Yee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
