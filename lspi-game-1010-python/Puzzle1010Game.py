import numpy as np
import random
import math
from piecesUtils import Pieces

class Game(object):

	def __init__(self, dimension):
		super(Game, self).__init__()
		self.dimension = dimension
		self.pieces = Pieces()
		self.fullX = np.zeros(dimension, dtype=int)
		self.fullY = np.zeros(dimension, dtype=int)

	def takeAction(self, x, y, piece, board):
		tmpBoard = np.copy(board)
		tmpFullX = np.copy(self.fullX)
		tmpFullY = np.copy(self.fullY)
		# print(x, y)
		allPiecesCoordinates = self.pieces.allPiecesCoordinates

		for co in allPiecesCoordinates[piece]:
			tmpBoard[x+co[0]][y+co[1]] = 1
			tmpFullX[x+co[0]] += 1
			tmpFullY[y+co[1]] += 1


		lineClearX = []
		lineClearY = []
		for x in range(self.dimension):
			if tmpFullX[x] == self.dimension:
				tmpFullX[x] = 0
				lineClearX += [x]
				for i in range(self.dimension):
					tmpBoard[x][i] = 0
					tmpFullY[i] -= 1

		for y in range(self.dimension):
			if tmpFullY[y] == self.dimension - len(lineClearX):
				tmpFullY[y] = 0
				lineClearY += [y]
				for i in range(self.dimension):
					tmpBoard[i][y] = 0
					if not i in lineClearX:
						tmpFullX[i] -= 1

		return tmpBoard, 10 * math.pow(len(lineClearX) + len(lineClearY), 2) + 1, tmpFullX, tmpFullY

	def random3Pieces(self):
		pieces = []
		num1 = self.randomPieces()
		num2 = self.randomPieces()
		num3 = self.randomPieces()

		pieces.append([num1, 0])
		pieces.append([num2, 0])
		pieces.append([num3, 0])
		pieces = np.array(pieces)

		return pieces

	def realGameOver(self, pieces, gameBoard):
		gameOver = True
		# print(pieces)
		for i in range(3):
			if pieces[i][1] == 0:
				if self.checkGameOver(pieces[i][0], gameBoard) == False:
					gameOver = False

		return gameOver

	def printAllpieces(self, pieceIndices):
		for p in pieceIndices:
			if p[1] == 0:
				self.pieces.printPieces(p[0])
				print("\n")

	def printBoard(self, board):
		print("===========================================")
		print("  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |")
		print("===========================================")

		for i in range(self.dimension):
			print(str(i) + " |", end='')
			for j in range(self.dimension):
				if board[i][j] == 1:
					print(" X |", end='')
				else:
					print("   |", end='')
			print()
			print("-------------------------------------------")

		# print(board)
		print("-------------------------------------------")

	def randomPieces(_):
		number = np.random.choice(np.arange(0, 18), p=[2/40, 3/40, 3/40, 2/40, 2/40, 2/40, 2/40, 6/40, 3/40, 2/40, 2/40, 3/40, 2/40, 2/40, 1/40, 1/40, 1/40, 1/40])
		# number = random.randint(0, 18)
		return number

	def checkValidAction(self, x, y, piece, board):
		allPiecesCoordinates = self.pieces.allPiecesCoordinates
		for co in allPiecesCoordinates[piece]:
			if x + co[0] >= self.dimension or y + co[1] >= self.dimension or board[x+co[0]][y+co[1]] != 0:
				return False
		return True

	def checkGameOver(self, piece, board):
		for x in range(self.dimension):
			for y in range(self.dimension):
				continueGame = self.checkValidAction(x, y, piece, board)
				if continueGame == True:
					return False
		return True
		