import numpy as np
import random
import math

# for demo uses
# all calculations are hidden

class PhiValueCalculator(object):

	def __init__(self, dimension):
		self.dimension = dimension
		self.averageHorizontalConsecustiveHoles = 1
		self.averageHorizontalConsecustiveHoles = 1
		self.maxHorizontalConsecutiveHoles = 1
		self.averageVerticalConsecustiveHoles = 1
		self.maxVerticalConsecutiveHoles = 1
		self.numberOfOcciupedCells = 1
		self.numberOfHoles = 1
		self.numberOfPossibleActionOfPuttingHorizontal5x1Piece = 1
		self.numberOfPossibleActionOfPuttingVertical5x1Piece = 1
		self.numberOfPossibleActionOfPutting3x3Square = 1
		self.maxAreaOfSquare = 1
		self.maxAreaOfRectangle = 1
		self.farFromEdge = 1
		self.numberOfPossibleActionOfPuttingHorizontal4x1Piece = 1
		self.numberOfPossibleActionOfPuttingVertical4x1Piece = 1
		self.numberOfPossibleActionOfPutting3x3Square = 1
		self.numberOfPossibleActionOfPuttingLShapeSE = 1
		self.numberOfPossibleActionOfPuttingLShapeNE = 1
		self.numberOfPossibleActionOfPuttingLShapeNW = 1
		self.numberOfPossibleActionOfPuttingLShapeSW = 1
		self.numberOfFullHorizontalLine = 1
		self.numberOfFullVerticalLine = 1
		self.numberOfPossibleActionOfPuttingVertical2x1Piece = 1
		self.numberOfPossibleActionOfPuttingSmallLSE = 1
		self.numberOfPossibleActionOfPuttingSmallLSW = 1
		self.numberOfPossibleActionOfPuttingSmallLNE = 1
		self.numberOfPossibleActionOfPutting2x2Square = 1
		self.numberOfPossibleActionOfPuttingHorizontal2x1Piece = 1
		self.numberOfPossibleActionOfPuttingHorizontal3x1Piece = 1
		self.numberOfPossibleActionOfPuttingVertical3x1Piece = 1
		self.numberOfPossibleActionOfPuttingSmallLNW = 1
		self.maxSquareLength = 1
		self.areaOfSquare = 1
		self.areaOfRectangle = 1
		self.ncomponentOfBlankSpace = 1
		self.ncomponentOfOccupiedSpace = 1
		self.maxBlankSpace = 1
		self.maxOccupiedSpace = 1
		self.numberOfPossibleActionOfPutting1x1Piece = 1
		self.exOfSurvival = 1
		self.maxXLength = 1
		self.maxYLength = 1

	def doCalculation(self, board):

		self.holesFunction(board)
		self.areaFunction(board, self.maxXLength, self.maxYLength)
		self.calculateIrregularSegment(board)
		self.calculateEmptyHoles(board)
		self.calculateExOfSurvival(board)
		return

	def holesFunction(_, board):
		# do some calculations of the below parameters from the game board

		#      averageHorizontalConsecustiveHoles,
		# 	   maxHorizontalConsecutiveHoles,
		# 	   averageVerticalConsecustiveHoles,
		# 	   maxVerticalConsecutiveHoles,
		# 	   numberOfOcciupedCells,
		# 	   numberOfHoles,
		# 	   numberOfPossibleActionOfPuttingHorizontal5x1Piece,
		# 	   numberOfPossibleActionOfPuttingVertical5x1Piece,
		# 	   numberOfPossibleActionOfPutting3x3Square,
		# 	   maxAreaOfSquare,maxAreaOfRectangle,
		# 	   farFromEdge,
		# 	   numberOfPossibleActionOfPuttingHorizontal4x1Piece,
		# 	   numberOfPossibleActionOfPuttingVertical4x1Piece,
		# 	   numberOfPossibleActionOfPutting3x3Square,
		# 	   numberOfPossibleActionOfPuttingLShapeSE,
		# 	   numberOfPossibleActionOfPuttingLShapeNE,
		# 	   numberOfPossibleActionOfPuttingLShapeNW,
		# 	   numberOfPossibleActionOfPuttingLShapeSW, 
		# 	   numberOfFullHorizontalLine, 
		# 	   numberOfFullVerticalLine, 
		# 	   numberOfPossibleActionOfPuttingVertical2x1Piece, 
		# 	   numberOfPossibleActionOfPuttingSmallLSE, 
		# 	   numberOfPossibleActionOfPuttingSmallLSW, 
		# 	   numberOfPossibleActionOfPuttingSmallLNE, 
		# 	   numberOfPossibleActionOfPutting2x2Square, 
		# 	   numberOfPossibleActionOfPuttingHorizontal2x1Piece, 
		# 	   numberOfPossibleActionOfPuttingHorizontal3x1Piece, 
		# 	   numberOfPossibleActionOfPuttingVertical3x1Piece, 
		# 	   numberOfPossibleActionOfPuttingSmallLNW

		return

	def areaFunction(_, board, maxXLength, maxYLength):
		# do some calculations of the below parameters from the game board

		#  maxSquareLength,
		#  areaOfSquare,
		#  areaOfRectangle,
		return


	def calculateIrregularSegment(_, board):
		# do some calculations of the below parameters from the game board

		# ncomponentOfBlankSpace,
		# ncomponentOfOccupiedSpace,
		# maxBlankSpace,
		# maxOccupiedSpace,
		return


	def calculateExOfSurvival(_, board):
		# do some calculations of the below parameters from the game board

		# exOfSurvival
		return

	def calculateEmptyHoles(_, board):
		# do some calculations of the below parameters from the game board

		# numberOfPossibleActionOfPutting1x1Piece
		return