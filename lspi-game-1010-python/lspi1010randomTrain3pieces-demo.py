import numpy as np
import random
import math
from piecesUtils import Pieces
from lspi1010PhiValueCalculation import PhiValueCalculator
from Puzzle1010Game import Game

dimension = 10
numberOfPieces = 7
gamma = 0.8
epsilon = 0.2
numberOfFeature = 21
gameBoard = np.zeros((dimension, dimension), dtype=int)
trainingMode = False

w = np.zeros((numberOfFeature,1), dtype=float)
A = np.zeros((numberOfFeature, numberOfFeature), dtype=float)
b = np.zeros((numberOfFeature,1), dtype=float)

phi1 = np.zeros((numberOfFeature,1), dtype=float)

count = 0

fullX = np.zeros(dimension, dtype=int)
fullY = np.zeros(dimension, dtype=int)

pieces = Pieces()
phiValueCalculator = PhiValueCalculator(dimension)
game = Game(dimension)


####### training #######


def calculateFeatureVector(state, r):
	phi = np.zeros((numberOfFeature,1), dtype=float)

	phiValueCalculator.doCalculation(gameBoard)

	phi[0][0] = phiValueCalculator.averageHorizontalConsecustiveHoles
	phi[1][0] = phiValueCalculator.maxHorizontalConsecutiveHoles
	phi[2][0] = phiValueCalculator.averageVerticalConsecustiveHoles
	phi[3][0] = phiValueCalculator.maxVerticalConsecutiveHoles
	phi[4][0] = phiValueCalculator.numberOfOcciupedCells
	phi[5][0] = phiValueCalculator.numberOfHoles
	phi[6][0] = phiValueCalculator.numberOfPossibleActionOfPuttingHorizontal5x1Piece
	phi[7][0] = phiValueCalculator.numberOfPossibleActionOfPuttingVertical5x1Piece
	phi[8][0] = phiValueCalculator.numberOfPossibleActionOfPutting3x3Square
	phi[9][0] = phiValueCalculator.maxAreaOfSquare
	phi[10][0] = phiValueCalculator.maxAreaOfRectangle
	phi[11][0] = phiValueCalculator.farFromEdge
	phi[12][0] = phiValueCalculator.numberOfFullHorizontalLine
	phi[13][0] = phiValueCalculator.numberOfFullVerticalLine
	phi[14][0] = phiValueCalculator.ncomponentOfBlankSpace
	phi[15][0] = phiValueCalculator.ncomponentOfOccupiedSpace
	phi[16][0] = phiValueCalculator.maxBlankSpace
	phi[17][0] = phiValueCalculator.maxOccupiedSpace
	phi[18][0] = r
	phi[19][0] = phiValueCalculator.exOfSurvival
	phi[20][0] = 1

	return phi



def calculateAllQValues(state, piece):
	global w
	
	if trainingMode == True and False:
		prob = random.random()
	else:
		prob = 1

	if prob > epsilon:
		S = []
		V = []
		TX = []
		TY = []
		allPhi = []
		haveMove = False
		for i in range(dimension):
				for j in range(dimension):
					if game.checkValidAction(i, j, piece, gameBoard):
						haveMove = True
						sd, r , tx, ty = game.takeAction(i, j, piece, gameBoard)
						#sd, r , tx, ty, phi = takeActionAndCalculateFeatureVector(i, j, piece, gameBoard)
						TX.append(tx)
						TY.append(ty)

						phi = calculateFeatureVector(sd, r)


						allPhi.append(phi)

						w = w.reshape(-1, numberOfFeature)
						# print(np.shape(phi), np.shape(w))
						value = np.dot(w, phi)
						# print(value)
						V.append(value)
						tmp = [state, piece, (i,j), r, sd]
						S.append(tmp)
		if haveMove:
			npV = np.array(V)
			maxV = max(V)
			maxVList = np.where(npV == maxV)[0]
			maxIndex = maxVList[np.random.choice(len(maxVList))]

			return S[maxIndex], TX[maxIndex], TY[maxIndex], allPhi[maxIndex], V[maxIndex]
		else:
			return None, None, None, None, -1000
	else:
		valid = False
		while not valid:
			x = random.randint(0,dimension-1)
			y = random.randint(0,dimension-1)
			valid = checkValidAction(x, y, piece, gameBoard)
			if valid == False:
				continue
			sd, r , tx, ty = takeAction(x, y, piece, gameBoard)

			phi = calculateFeatureVector(sd, r)

			tmp = [state, piece, (x,y), r, sd]
			return tmp, tx, ty, phi

def updateAandb(sample, phi2):
	global A
	global b
	global w
	global phi1
	s = sample[0]

	r = sample[3]
	sd = sample[4]

	tmp = phi1 - phi2 * gamma

	A = A + np.dot(phi1, np.transpose(tmp))
	b = b + phi1 * r

	phi1 = phi2

def updateWeight():
	global w
	w = np.dot(np.linalg.pinv(A),b)



def compareAndFindMaxValueOfEachPieces(pieces, gameBoard):
	samples = []
	TXs = []
	TYs = []
	phis = []
	values = []
	
	for i in range(3):
		if pieces[i][1] == 0:
			sample, TX, TY, phi, value = calculateAllQValues(gameBoard, pieces[i][0])
			samples.append(sample)
			TXs.append(TX)
			TYs.append(TY)
			phis.append(phi)
			values.append(value)
		else:
			samples.append(None)
			TXs.append(None)
			TYs.append(None)
			phis.append(None)
			values.append(-1000)


	index = np.argmax(values)

	return samples[index], TXs[index], TYs[index], phis[index], index


#### game related ###


def startARound():
	global gameBoard
	global count
	global fullX
	global fullY
	print("------------startARound---------------")
	if trainingMode == False:
		game.printBoard(gameBoard)
	Round = 0
	# piece = randomPieces()
	pieces = game.random3Pieces()

	score = 0
	nextBoard = np.copy(gameBoard)

	while not game.realGameOver(pieces, gameBoard):
		# Round += 1
		# count += 1
		numberOfPiecesLeft = 3
		game.printAllpieces(pieces)

		for i in range(3):
			sample, TX, TY, phi, index = compareAndFindMaxValueOfEachPieces(pieces, gameBoard)
			gameBoard = sample[4]
			fullX = TX
			fullY = TY
			score += sample[3]

			if trainingMode == False:
				
				print("action = ", sample[2][0], sample[2][1])
				game.printBoard(gameBoard)

			print(index)
			pieces[index][1] = -1
			game.printAllpieces(pieces)
			numberOfPiecesLeft -= 1
			Round += 1
			# print("Round = ", Round)
			go = game.realGameOver(pieces, gameBoard)
			if go == True and numberOfPiecesLeft != 0:
				print("break")
				break

		if numberOfPiecesLeft == 0:
			pieces = game.random3Pieces()
		else:
			break
	print(Round)
	game.printAllpieces(pieces)
	print("GameOver")

	return True

def startGame():
	global gameBoard
	global fullX
	global fullY
	gameBoard = np.zeros((dimension, dimension), dtype=int)
	fullX = np.zeros(dimension, dtype=int)
	fullY = np.zeros(dimension, dtype=int)

	gameOver = False
	while gameOver != True:
		gameOver = startARound()

def saveWeight(i):
	filename = "weight" + str(i) + ".txt"
	with open(filename, 'w') as f:
		f.write(str(i))
		for j in w:
			f.write("," + str(j[0]))

# # an example of the training result of the weight function
# w =  np.array( [4.52319913e+01,
#  -3.73481415e-01,
#   3.93335780e+01,
#   1.07828188e+00,
#   2.15048389e+00,
#   4.06361430e+00,
#  -3.06207897e+00,
#  -3.14567301e+00,
#  -1.88508407e+00,
#   4.34376149e-01,
#   4.67086741e-01,
#  -1.61435903e-02,
#   1.37288589e+00,
#  -3.00732054e-01,
#  -6.52155829e+00,
#   1.26302568e-01,
#  -4.55051739e-01,
#  -1.60360022e-02,
#   1.08772290e-01,
#   1.28052592e+03,
#  -1.66964610e+03]
# )

if trainingMode == True:
	for i in range(1001):
		if i % 100 == 0:
			saveWeight(i)
		if i % 20 == 0:
			print(w)
		print("Turn = " + str(i))
		startGame()
else:
	startGame()



